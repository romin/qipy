#! usr/bin/python
# coding=utf-8
import os
import re
import urllib
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler


# 自定义处理程序，用于处理HTTP请求
class TestHTTPHandler(BaseHTTPRequestHandler):
    # 处理GET请求
    def do_GET(self):
        # 页面输出模板字符串
        templateStr = '''<html>
<head>
<title>test</title>
</head>
<body>
%s
</body>
</html> '''
        # print self.headers
        self.protocal_version = 'HTTP/1.1'  # 设置协议版本
        self.send_response(200)  # 设置响应状态码
        self.send_header("Welcome", "Contect")  # 设置响应头
        self.end_headers()
        print '-----------request---------------'
        print self.headers
        self.wfile.write(templateStr % "hello word")  # 输出响应内容


# 启动服务函数
def start_server(port):
    http_server = HTTPServer(('', int(port)), TestHTTPHandler)
    http_server.serve_forever()  # 设置一直监听并接收请求


if __name__ == '__main__':
    os.chdir('static')  # 改变工作目录到 static 目录
    print("aaa\n")
    start_server(8000)  # 启动服务，监听8000端口
